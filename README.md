﻿# 智付通SPGateway(藍新) 金流API Sample Code
第三方金流智付通SPGateway(藍新) API串接Sample Code

智付通(藍新)網址: https://www.newebpay.com
智付通(藍新)測試網址: https://cwww.newebpay.com

---

## 實作功能

 - [多功能收款 MPG-程式版本號:1.5](https://www.newebpay.com/website/Page/download_file?name=%E6%99%BA%E4%BB%98%E9%80%9ASpgateway_MPG%E4%B8%B2%E6%8E%A5%E6%89%8B%E5%86%8A_%E6%A8%99%E6%BA%96%E7%89%88_Spgateway_MPG_1.1.220181025.pdf)
- [信用卡請退款-程式版本號1.0](https://www.newebpay.com/website/Page/download_file?name=%E8%97%8D%E6%96%B0%E9%87%91%E6%B5%81Newebpay_Gateway%E4%BF%A1%E7%94%A8%E5%8D%A1%E8%AB%8B%E9%80%80%E6%AC%BE%E4%B8%B2%E6%8E%A5%E6%89%8B%E5%86%8A%20V1.0.0.pdf)

> 連結失效可參考Document資料夾下檔案

## 開發環境
- Microsoft Visual Studio 2017(.net Framework 4.71)

## 說明
- `執行前`請先填入個人的MerchantID、HashKey、HashIV、ReturnURL、NotifyURL、CustomerURL
    ```csharp
    // BankController
    private BankInfoModel _bankInfoModel = new BankInfoModel
    {
        MerchantID = "MS00000000",
        HashKey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        HashIV = "xxxxxxxxxxxx",
        ReturnURL = "http://yourWebsitUrl/Bank/SpgatewayReturn",
        NotifyURL = "http://yourWebsitUrl/Bank/SpgatewayNotify",
        CustomerURL = "http://yourWebsitUrl/Bank/SpgatewayCustomer",
        AuthUrl = "https://ccore.spgateway.com/MPG/mpg_gateway",
        CloseUrl = "https://core.newebpay.com/API/CreditCard/Close"
    };
  ```
  
- MPG 交易可直接參考BankController -> SpgatewayPayBill
-  MPG (ReturlUrl) 支付完成返回商店處理可直接參考BankController -> SpgatewayReturn
-  MPG (CustomerUrl) 商店取號網址處理可直接參考BankController -> SpgatewayCustomer
- 請退款API串接可參考 BankController -> SpgatewayClose
- 交易所需的 AES 加解密與SHA可參考`CryptoUtil.cs`
